const Joi = require('joi');

module.exports.validateRegister = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
              .email(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),

    role: Joi.string()
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validateLogin = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
              .email(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),

  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validateEmail = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
              .email(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validateUser = async (req, res, next) => {
  const schema = Joi.object({
    _id: Joi.string(),
    email: Joi.string()
              .email(),
    created_date: Joi.string(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validateTruck = async (req, res, next) => {
  const schema = Joi.object({
    _id: Joi.string(),
    created_by: Joi.string(),
    assigned_to: Joi.string(),
    type: Joi.string().required(),
    status: Joi.string(),
    createdDate: Joi.string(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validateLoad = async (req, res, next) => {
  const schema = Joi.object({
    _id: Joi.string(),
    name: Joi.string().required(),
    created_by: Joi.string(),
    assigned_to: Joi.string(),
    payload: Joi.number(),
    pickup_address: Joi.string(),
    delivery_address: Joi.string(),
    dimensions: Joi.object(),
    createdDate: Joi.string(),
  });

  await schema.validateAsync(req.body);
  next();
};