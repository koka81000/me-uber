const mongoose = require('mongoose');

const credentialSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
});

module.exports.Credential = mongoose.model('Credential', credentialSchema);
