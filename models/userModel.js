const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
  },

  created_date: {
    type: String,
    default: new Date().toISOString(),
  },
});

module.exports.User = mongoose.model('User', userSchema);
